import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 10 * 20 ";
        assertEquals("200", calculatorResource.calculate(expression));

        expression = " 10/5 ";
        assertEquals("2", calculatorResource.calculate(expression));

    }

    @Test
    public void testCalculateThreeDigits(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+100";
        assertEquals("500", calculatorResource.calculate(expression));

        expression = " 300 - 99 - 2";
        assertEquals("199", calculatorResource.calculate(expression));

        expression = " 10 * 20 * 2 ";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 10/5/2 ";
        assertEquals("1", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "20*10";
        assertEquals(200, calculatorResource.multiplication(expression));

        expression = "5*20";
        assertEquals(100, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/5";
        assertEquals(20, calculatorResource.division(expression));

        expression = "25/5";
        assertEquals(5, calculatorResource.division(expression));
    }
}
